using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using UnityEngine;

namespace LS
{
    public class Launcher : MonoBehaviour
    {
        [SerializeField] GameObject Ball_Go;
        [SerializeField] Transform ObjTrans;
        [SerializeField] Rigidbody Ballrb;

        public float H = 10;
        public float gravity = -9.8f;
        [Space]

        [Range(1,10)] public float numPuntos; 

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Lanzar();
            }
        }

        private void Lanzar()
        {

            Physics.gravity = Vector3.up * gravity;

            Ballrb.useGravity = true;
            Ballrb.velocity = CalcularVelocidadInicial();
            
            print(H);

        }

        private Vector3 CalcularVelocidadInicial()
        {
            Vector3 desplazaminetoP = ObjTrans.position - Ball_Go.transform.position;

            //Velocidad inicial 
            float velY, velZ, velX;

            velY = Mathf.Sqrt(-2 * gravity * H);

            velX = desplazaminetoP.x / ((-velY / gravity) + Mathf.Sqrt(2 * (desplazaminetoP.y - H) / gravity));

            velZ = desplazaminetoP.z / ((-velY / gravity) + Mathf.Sqrt(2 * (desplazaminetoP.y - H) / gravity));

            return new Vector3(velX, velY, velZ);
        }

        //private void OnDrawGizmos()
        //{
        //    Gizmos.color = Color.yellow;

        //    DibujarLineas();
        //}

        //private void DibujarLineas()
        //{
        //    Vector3 desde, hacia;
        //    Vector3 VelInicial = CalcularVelocidadInicial();
           
        //    float TiempoTotal = CalcularTiempoTotal(VelInicial.y);
        //    float TiempoActual = 0;
        //    float paso = TiempoTotal/numPuntos;

        //    hacia = Ball_Go.transform.position;

        //    while (TiempoActual <= TiempoTotal)
        //    {
        //        desde = hacia; 

        //        hacia = CalcularTrayectoria(VelInicial, TiempoActual);

        //        Gizmos.DrawLine(desde,hacia);

        //        TiempoActual += paso;
        //    }
        //}

        //private Vector3 CalcularTrayectoria(Vector3 velInicial, float Time)
        //{
        //    float desplazaminetoY, desplazaminetoX, desplazaminetoZ;

        //    desplazaminetoY = velInicial.y * Time + gravity * Time * Time / 2;
        //    desplazaminetoX = velInicial.x * Time;
        //    desplazaminetoZ = velInicial.z * Time;

        //    return new Vector3(desplazaminetoX, desplazaminetoY, desplazaminetoZ);
        //}

        //float CalcularTiempoTotal(float VelInicialY) => -VelInicialY / gravity + MathF.Sqrt(2 * (ObjTrans.position.y - H) / gravity);
        
    }
}
