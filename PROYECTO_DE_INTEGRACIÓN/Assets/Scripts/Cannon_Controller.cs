using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LS
{
    public class Cannon_Controller : MonoBehaviour
    {
        [Header("Ca�on")]
        public float rotationSpeed = 1;
        public float blastPower = 5;
        [SerializeField] Transform ObjTrans;
        [SerializeField] GameObject Ca�on_girar;

        [Header("Bola")]
        public GameObject CannonBall;
        public Transform ShotPoint;

        [Header("Fisicas")]
        public float H = 10;
        public float gravity = -9.8f;
        
        [Space]
        public float HorizontalRotation2;
        public float VerticalRotation2;

        //Efectos
        //public GameObject Explosion;

        private void Start()
        {
            HorizontalRotation2 = 0;
        }

        private void Update()
        {
            float HorizontalRotation = Input.GetAxis("Horizontal");
            float VerticalRotation = Input.GetAxis("Vertical");

            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0, HorizontalRotation * rotationSpeed, VerticalRotation * rotationSpeed));

            if (Input.GetKeyDown(KeyCode.Q))
            {
                Lanzar();

            }

        }

        public void Lanzar()
        {

            Physics.gravity = Vector3.up * gravity;
            GameObject CreatedCannonBall = Instantiate(CannonBall, ShotPoint.position, ShotPoint.rotation);
            CreatedCannonBall.GetComponent<Rigidbody>().useGravity = true;
            CreatedCannonBall.GetComponent<Rigidbody>().velocity = ShotPoint.transform.up * blastPower;
            Destroy(CreatedCannonBall, 10f);

        }



    }
}
