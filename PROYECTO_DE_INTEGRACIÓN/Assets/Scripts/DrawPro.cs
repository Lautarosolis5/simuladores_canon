using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LS
{
    public class DrawPro : MonoBehaviour
    {
        Cannon_Controller cannon_Controller;
        LineRenderer lineRenderer;

        public int numPoints = 50;
        public float timeBetweenPoints = 0.1f;

        public LayerMask CollidableLayers;

        private void Start()
        {
            cannon_Controller = GetComponent<Cannon_Controller>();
            lineRenderer = GetComponent<LineRenderer>();
        }

        private void Update()
        {
            lineRenderer.positionCount = numPoints;
            List<Vector3> points = new List<Vector3>();
            Vector3 startingPosition = cannon_Controller.ShotPoint.position;
            Vector3 startingVelocity = cannon_Controller.ShotPoint.up * cannon_Controller.blastPower;

            for (float t = 0; t < numPoints; t += timeBetweenPoints)
            {
                Vector3 newPoint = startingPosition + t * startingVelocity;
                newPoint.y = startingPosition.y + startingVelocity.y * t + cannon_Controller.gravity / 2f * t * t;
                points.Add(newPoint);

                if(Physics.OverlapSphere(newPoint,2,CollidableLayers).Length > 0)
                {
                    lineRenderer.positionCount = points.Count;
                    break;
                }
            }

            lineRenderer.SetPositions(points.ToArray());
        }
    }
}
