using LS;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

public class SCP_guiJuego : MonoBehaviour
{

    [SerializeField]
    private UIDocument m_UIDocument;
    private VisualElement interfazPrincipal;
    public GameObject cannon;
    private Button disparar;
    private Scroller scroller;
    private Scroller Scroll_Potencia;

    void Start()
    {
        interfazPrincipal = m_UIDocument.rootVisualElement;

        scroller = interfazPrincipal.Q<Scroller>("Scroll_Altura");
        scroller.lowValue = -50f;
        scroller.highValue = 50f;
        scroller.value = -9.8f;
        scroller.valueChanged += (evt) => { OnScrollerValueChanged(); };

        Scroll_Potencia = interfazPrincipal.Q<Scroller>("Scroll_Potencia");
        Scroll_Potencia.slider.inverted = false;
        Scroll_Potencia.valueChanged += (evt) => { OnScrollValuePowerChange(); };

        disparar = interfazPrincipal.Q<Button>("btn_Disparar");
        disparar.clickable.clicked += () => { cannon.GetComponent<Cannon_Controller>().Lanzar(); };
            

    }

    private void OnScrollerValueChanged()
    {
        cannon.GetComponent<Cannon_Controller>().gravity = scroller.value;
        Debug.Log("Nuevo valor del Scroller: " );
    }

    private void OnScrollValuePowerChange()
    {
        cannon.GetComponent<Cannon_Controller>().blastPower = Scroll_Potencia.value;
        Debug.Log("Nuevo valor del Scroller: ");
    }
}


